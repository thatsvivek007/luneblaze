<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QuizzesQuestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QuizzesQuestionsTable Test Case
 */
class QuizzesQuestionsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\QuizzesQuestionsTable
     */
    public $QuizzesQuestions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.QuizzesQuestions',
        'app.Quizzes',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('QuizzesQuestions') ? [] : ['className' => QuizzesQuestionsTable::class];
        $this->QuizzesQuestions = TableRegistry::getTableLocator()->get('QuizzesQuestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->QuizzesQuestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
