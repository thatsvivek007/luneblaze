<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QuizzesQuestionsOptionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QuizzesQuestionsOptionsTable Test Case
 */
class QuizzesQuestionsOptionsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\QuizzesQuestionsOptionsTable
     */
    public $QuizzesQuestionsOptions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.QuizzesQuestionsOptions',
        'app.Quizzes',
        'app.Questions',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('QuizzesQuestionsOptions') ? [] : ['className' => QuizzesQuestionsOptionsTable::class];
        $this->QuizzesQuestionsOptions = TableRegistry::getTableLocator()->get('QuizzesQuestionsOptions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->QuizzesQuestionsOptions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
