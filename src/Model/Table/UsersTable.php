<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\UserPicturesTable&\Cake\ORM\Association\BelongsTo $UserPictures
 * @property \App\Model\Table\UserCoversTable&\Cake\ORM\Association\BelongsTo $UserCovers
 * @property \App\Model\Table\FacebooksTable&\Cake\ORM\Association\BelongsTo $Facebooks
 * @property \App\Model\Table\TwittersTable&\Cake\ORM\Association\BelongsTo $Twitters
 * @property \App\Model\Table\GooglesTable&\Cake\ORM\Association\BelongsTo $Googles
 * @property \App\Model\Table\InstagramsTable&\Cake\ORM\Association\BelongsTo $Instagrams
 * @property \App\Model\Table\LinkedinsTable&\Cake\ORM\Association\BelongsTo $Linkedins
 * @property \App\Model\Table\VkontaktesTable&\Cake\ORM\Association\BelongsTo $Vkontaktes
 * @property \App\Model\Table\UserReferrersTable&\Cake\ORM\Association\BelongsTo $UserReferrers
 * @property \App\Model\Table\OraganisationsTable&\Cake\ORM\Association\BelongsTo $Oraganisations
 * @property \App\Model\Table\AnnouncementsTable&\Cake\ORM\Association\BelongsToMany $Announcements
 * @property \App\Model\Table\ConversationsTable&\Cake\ORM\Association\BelongsToMany $Conversations
 * @property \App\Model\Table\CustomFieldsTable&\Cake\ORM\Association\BelongsToMany $CustomFields
 * @property \App\Model\Table\SessionsTable&\Cake\ORM\Association\BelongsToMany $Sessions
 * @property \App\Model\Table\SessionsPollsOptionsTable&\Cake\ORM\Association\BelongsToMany $SessionsPollsOptions
 * @property \App\Model\Table\VenueTable&\Cake\ORM\Association\BelongsToMany $Venue
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('user_id');
        $this->setPrimaryKey('user_id');

        $this->belongsTo('UserPictures', [
            'foreignKey' => 'user_picture_id',
        ]);
        $this->belongsTo('UserCovers', [
            'foreignKey' => 'user_cover_id',
        ]);
        $this->belongsTo('Facebooks', [
            'foreignKey' => 'facebook_id',
        ]);
        $this->belongsTo('Twitters', [
            'foreignKey' => 'twitter_id',
        ]);
        $this->belongsTo('Googles', [
            'foreignKey' => 'google_id',
        ]);
        $this->belongsTo('Instagrams', [
            'foreignKey' => 'instagram_id',
        ]);
        $this->belongsTo('Linkedins', [
            'foreignKey' => 'linkedin_id',
        ]);
        $this->belongsTo('Vkontaktes', [
            'foreignKey' => 'vkontakte_id',
        ]);
        $this->belongsTo('UserReferrers', [
            'foreignKey' => 'user_referrer_id',
        ]);
        $this->belongsTo('Oraganisations', [
            'foreignKey' => 'oraganisation_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsToMany('Announcements', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'announcement_id',
            'joinTable' => 'announcements_users',
        ]);
        $this->belongsToMany('Conversations', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'conversation_id',
            'joinTable' => 'conversations_users',
        ]);
        $this->belongsToMany('CustomFields', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'custom_field_id',
            'joinTable' => 'users_custom_fields',
        ]);
        $this->belongsToMany('Sessions', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'session_id',
            'joinTable' => 'users_sessions',
        ]);
        $this->belongsToMany('SessionsPollsOptions', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'sessions_polls_option_id',
            'joinTable' => 'users_sessions_polls_options',
        ]);
        $this->belongsToMany('Venue', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'venue_id',
            'joinTable' => 'users_venue',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('user_id')
            ->allowEmptyString('user_id', null, 'create');

        $validator
            ->notEmptyString('user_group');

        $validator
            ->scalar('user_name')
            ->maxLength('user_name', 64)
            ->requirePresence('user_name', 'create')
            ->notEmptyString('user_name')
            ->add('user_name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('user_email')
            ->maxLength('user_email', 64)
            ->requirePresence('user_email', 'create')
            ->notEmptyString('user_email');

        $validator
            ->scalar('user_phone')
            ->maxLength('user_phone', 64)
            ->allowEmptyString('user_phone');

        $validator
            ->scalar('user_password')
            ->maxLength('user_password', 64)
            ->requirePresence('user_password', 'create')
            ->notEmptyString('user_password');

        $validator
            ->scalar('current_FOI')
            ->maxLength('current_FOI', 1000)
            ->allowEmptyString('current_FOI');

        $validator
            ->scalar('user_latitude')
            ->maxLength('user_latitude', 50)
            ->requirePresence('user_latitude', 'create')
            ->notEmptyString('user_latitude');

        $validator
            ->scalar('user_longitude')
            ->maxLength('user_longitude', 50)
            ->requirePresence('user_longitude', 'create')
            ->notEmptyString('user_longitude');

        $validator
            ->scalar('user_activated')
            ->notEmptyString('user_activated');

        $validator
            ->scalar('user_activation_key')
            ->maxLength('user_activation_key', 64)
            ->allowEmptyString('user_activation_key');

        $validator
            ->scalar('user_reseted')
            ->notEmptyString('user_reseted');

        $validator
            ->scalar('user_reset_key')
            ->maxLength('user_reset_key', 64)
            ->allowEmptyString('user_reset_key');

        $validator
            ->scalar('user_subscribed')
            ->notEmptyString('user_subscribed');

        $validator
            ->nonNegativeInteger('user_package')
            ->allowEmptyString('user_package');

        $validator
            ->dateTime('user_subscription_date')
            ->allowEmptyDateTime('user_subscription_date');

        $validator
            ->nonNegativeInteger('user_boosted_posts')
            ->notEmptyString('user_boosted_posts');

        $validator
            ->nonNegativeInteger('user_boosted_pages')
            ->notEmptyString('user_boosted_pages');

        $validator
            ->scalar('user_started')
            ->notEmptyString('user_started');

        $validator
            ->scalar('user_verified')
            ->notEmptyString('user_verified');

        $validator
            ->scalar('user_banned')
            ->notEmptyString('user_banned');

        $validator
            ->nonNegativeInteger('user_live_requests_counter')
            ->notEmptyString('user_live_requests_counter');

        $validator
            ->nonNegativeInteger('user_live_requests_lastid')
            ->notEmptyString('user_live_requests_lastid');

        $validator
            ->nonNegativeInteger('user_live_messages_counter')
            ->notEmptyString('user_live_messages_counter');

        $validator
            ->nonNegativeInteger('user_live_messages_lastid')
            ->notEmptyString('user_live_messages_lastid');

        $validator
            ->nonNegativeInteger('user_live_notifications_counter')
            ->notEmptyString('user_live_notifications_counter');

        $validator
            ->nonNegativeInteger('user_live_notifications_lastid')
            ->notEmptyString('user_live_notifications_lastid');

        $validator
            ->integer('user_sessions_counter')
            ->notEmptyString('user_sessions_counter');

        $validator
            ->scalar('user_firstname')
            ->maxLength('user_firstname', 255)
            ->requirePresence('user_firstname', 'create')
            ->notEmptyString('user_firstname');

        $validator
            ->scalar('user_lastname')
            ->maxLength('user_lastname', 255)
            ->allowEmptyString('user_lastname');

        $validator
            ->scalar('user_fullname')
            ->maxLength('user_fullname', 255)
            ->requirePresence('user_fullname', 'create')
            ->notEmptyString('user_fullname');

        $validator
            ->scalar('user_gender')
            ->requirePresence('user_gender', 'create')
            ->notEmptyString('user_gender');

        $validator
            ->scalar('user_picture')
            ->maxLength('user_picture', 255)
            ->allowEmptyString('user_picture');

        $validator
            ->scalar('user_pic_dimensions')
            ->maxLength('user_pic_dimensions', 20)
            ->requirePresence('user_pic_dimensions', 'create')
            ->notEmptyString('user_pic_dimensions');

        $validator
            ->scalar('user_cover')
            ->maxLength('user_cover', 255)
            ->allowEmptyString('user_cover');

        $validator
            ->scalar('user_cover_dimensions')
            ->maxLength('user_cover_dimensions', 20)
            ->requirePresence('user_cover_dimensions', 'create')
            ->notEmptyString('user_cover_dimensions');

        $validator
            ->nonNegativeInteger('user_album_pictures')
            ->allowEmptyString('user_album_pictures');

        $validator
            ->nonNegativeInteger('user_album_covers')
            ->allowEmptyString('user_album_covers');

        $validator
            ->nonNegativeInteger('user_album_timeline')
            ->allowEmptyString('user_album_timeline');

        $validator
            ->nonNegativeInteger('user_pinned_post')
            ->allowEmptyString('user_pinned_post');

        $validator
            ->dateTime('user_registered')
            ->allowEmptyDateTime('user_registered');

        $validator
            ->dateTime('user_last_login')
            ->allowEmptyDateTime('user_last_login');

        $validator
            ->date('user_birthdate')
            ->allowEmptyDate('user_birthdate');

        $validator
            ->scalar('user_relationship')
            ->maxLength('user_relationship', 255)
            ->allowEmptyString('user_relationship');

        $validator
            ->scalar('user_biography')
            ->allowEmptyString('user_biography');

        $validator
            ->scalar('user_website')
            ->maxLength('user_website', 255)
            ->allowEmptyString('user_website');

        $validator
            ->scalar('user_work')
            ->requirePresence('user_work', 'create')
            ->notEmptyString('user_work');

        $validator
            ->scalar('user_work_title')
            ->maxLength('user_work_title', 255)
            ->allowEmptyString('user_work_title');

        $validator
            ->scalar('user_work_place')
            ->maxLength('user_work_place', 255)
            ->allowEmptyString('user_work_place');

        $validator
            ->scalar('user_current_city')
            ->maxLength('user_current_city', 255)
            ->allowEmptyString('user_current_city');

        $validator
            ->scalar('user_hometown')
            ->maxLength('user_hometown', 255)
            ->allowEmptyString('user_hometown');

        $validator
            ->scalar('user_edu_major')
            ->maxLength('user_edu_major', 255)
            ->allowEmptyString('user_edu_major');

        $validator
            ->scalar('user_edu_school')
            ->maxLength('user_edu_school', 255)
            ->allowEmptyString('user_edu_school');

        $validator
            ->scalar('user_edu_class')
            ->maxLength('user_edu_class', 255)
            ->allowEmptyString('user_edu_class');

        $validator
            ->scalar('user_social_facebook')
            ->maxLength('user_social_facebook', 255)
            ->allowEmptyString('user_social_facebook');

        $validator
            ->scalar('user_social_twitter')
            ->maxLength('user_social_twitter', 255)
            ->allowEmptyString('user_social_twitter');

        $validator
            ->scalar('user_social_google')
            ->maxLength('user_social_google', 255)
            ->allowEmptyString('user_social_google');

        $validator
            ->scalar('user_social_youtube')
            ->maxLength('user_social_youtube', 255)
            ->allowEmptyString('user_social_youtube');

        $validator
            ->scalar('user_social_instagram')
            ->maxLength('user_social_instagram', 255)
            ->allowEmptyString('user_social_instagram');

        $validator
            ->scalar('user_social_linkedin')
            ->maxLength('user_social_linkedin', 255)
            ->allowEmptyString('user_social_linkedin');

        $validator
            ->scalar('user_social_vkontakte')
            ->maxLength('user_social_vkontakte', 255)
            ->allowEmptyString('user_social_vkontakte');

        $validator
            ->scalar('user_chat_enabled')
            ->notEmptyString('user_chat_enabled');

        $validator
            ->scalar('user_privacy_wall')
            ->notEmptyString('user_privacy_wall');

        $validator
            ->scalar('user_privacy_birthdate')
            ->notEmptyString('user_privacy_birthdate');

        $validator
            ->scalar('user_privacy_relationship')
            ->notEmptyString('user_privacy_relationship');

        $validator
            ->scalar('user_privacy_basic')
            ->notEmptyString('user_privacy_basic');

        $validator
            ->scalar('user_privacy_work')
            ->notEmptyString('user_privacy_work');

        $validator
            ->scalar('user_privacy_location')
            ->notEmptyString('user_privacy_location');

        $validator
            ->scalar('user_privacy_education')
            ->notEmptyString('user_privacy_education');

        $validator
            ->scalar('user_privacy_other')
            ->notEmptyString('user_privacy_other');

        $validator
            ->scalar('user_privacy_friends')
            ->notEmptyString('user_privacy_friends');

        $validator
            ->scalar('user_privacy_photos')
            ->notEmptyString('user_privacy_photos');

        $validator
            ->scalar('user_privacy_pages')
            ->notEmptyString('user_privacy_pages');

        $validator
            ->scalar('user_privacy_groups')
            ->notEmptyString('user_privacy_groups');

        $validator
            ->scalar('email_post_likes')
            ->notEmptyString('email_post_likes');

        $validator
            ->scalar('email_post_comments')
            ->notEmptyString('email_post_comments');

        $validator
            ->scalar('email_post_shares')
            ->notEmptyString('email_post_shares');

        $validator
            ->scalar('email_wall_posts')
            ->notEmptyString('email_wall_posts');

        $validator
            ->scalar('email_mentions')
            ->notEmptyString('email_mentions');

        $validator
            ->scalar('email_profile_visits')
            ->notEmptyFile('email_profile_visits');

        $validator
            ->scalar('email_friend_requests')
            ->notEmptyString('email_friend_requests');

        $validator
            ->scalar('facebook_connected')
            ->notEmptyString('facebook_connected');

        $validator
            ->scalar('twitter_connected')
            ->notEmptyString('twitter_connected');

        $validator
            ->scalar('google_connected')
            ->notEmptyString('google_connected');

        $validator
            ->scalar('instagram_connected')
            ->notEmptyString('instagram_connected');

        $validator
            ->scalar('linkedin_connected')
            ->notEmptyString('linkedin_connected');

        $validator
            ->scalar('vkontakte_connected')
            ->notEmptyString('vkontakte_connected');

        $validator
            ->scalar('user_affiliate_balance')
            ->maxLength('user_affiliate_balance', 64)
            ->notEmptyString('user_affiliate_balance');

        $validator
            ->scalar('fb_token')
            ->maxLength('fb_token', 255)
            ->allowEmptyString('fb_token');

        $validator
            ->scalar('google_token')
            ->maxLength('google_token', 255)
            ->allowEmptyString('google_token');

        $validator
            ->scalar('tw_token')
            ->maxLength('tw_token', 255)
            ->allowEmptyString('tw_token');

        $validator
            ->scalar('privacy_connection_request')
            ->notEmptyString('privacy_connection_request');

        $validator
            ->scalar('privacy_message')
            ->notEmptyString('privacy_message');

        $validator
            ->scalar('privacy_choices')
            ->notEmptyString('privacy_choices');

        $validator
            ->scalar('privacy_content')
            ->notEmptyString('privacy_content');

        $validator
            ->scalar('privacy_tag')
            ->notEmptyString('privacy_tag');

        $validator
            ->scalar('privacy_session')
            ->notEmptyString('privacy_session');

        $validator
            ->scalar('privacy_invite_session')
            ->notEmptyString('privacy_invite_session');

        $validator
            ->scalar('privacy_invite_conduct_session')
            ->notEmptyString('privacy_invite_conduct_session');

        $validator
            ->scalar('privacy_invite_answer')
            ->notEmptyString('privacy_invite_answer');

        $validator
            ->scalar('privacy_invite_interest')
            ->notEmptyString('privacy_invite_interest');

        $validator
            ->scalar('privacy_invite_venue')
            ->notEmptyString('privacy_invite_venue');

        $validator
            ->scalar('privacy_notification_email_invites_messages')
            ->notEmptyString('privacy_notification_email_invites_messages');

        $validator
            ->scalar('privacy_notification_email_sessions')
            ->notEmptyString('privacy_notification_email_sessions');

        $validator
            ->scalar('privacy_notification_email_qa')
            ->notEmptyString('privacy_notification_email_qa');

        $validator
            ->scalar('privacy_notification_email_articles')
            ->notEmptyString('privacy_notification_email_articles');

        $validator
            ->scalar('privacy_notification_email_update')
            ->notEmptyString('privacy_notification_email_update');

        $validator
            ->scalar('privacy_notification_push_invites_messages')
            ->notEmptyString('privacy_notification_push_invites_messages');

        $validator
            ->scalar('privacy_notification_push_sessions')
            ->notEmptyString('privacy_notification_push_sessions');

        $validator
            ->scalar('privacy_notification_push_qa')
            ->notEmptyString('privacy_notification_push_qa');

        $validator
            ->scalar('privacy_notification_push_articles')
            ->notEmptyString('privacy_notification_push_articles');

        $validator
            ->scalar('privacy_notification_push_update')
            ->notEmptyString('privacy_notification_push_update');

        $validator
            ->notEmptyString('noti_sound');

        $validator
            ->scalar('two_step_verifications')
            ->maxLength('two_step_verifications', 3)
            ->notEmptyString('two_step_verifications');

        $validator
            ->integer('login_otp')
            ->requirePresence('login_otp', 'create')
            ->notEmptyString('login_otp');

        $validator
            ->scalar('verify_otp')
            ->maxLength('verify_otp', 255)
            ->requirePresence('verify_otp', 'create')
            ->notEmptyString('verify_otp');

        $validator
            ->integer('people_influenced')
            ->requirePresence('people_influenced', 'create')
            ->notEmptyString('people_influenced');

        $validator
            ->date('delete_request_on')
            ->requirePresence('delete_request_on', 'create')
            ->notEmptyDate('delete_request_on');

        $validator
            ->integer('delete_withdraw')
            ->requirePresence('delete_withdraw', 'create')
            ->notEmptyString('delete_withdraw');

        $validator
            ->scalar('paymentplan')
            ->maxLength('paymentplan', 200)
            ->requirePresence('paymentplan', 'create')
            ->notEmptyString('paymentplan');

        $validator
            ->integer('interest_block')
            ->requirePresence('interest_block', 'create')
            ->notEmptyString('interest_block');

        $validator
            ->integer('quiz_count')
            ->requirePresence('quiz_count', 'create')
            ->notEmptyString('quiz_count');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['user_name']));
        $rules->add($rules->existsIn(['facebook_id'], 'Facebooks'));
        $rules->add($rules->existsIn(['twitter_id'], 'Twitters'));
        $rules->add($rules->existsIn(['google_id'], 'Googles'));
        $rules->add($rules->existsIn(['linkedin_id'], 'Linkedins'));
        $rules->add($rules->existsIn(['vkontakte_id'], 'Vkontaktes'));
        $rules->add($rules->existsIn(['instagram_id'], 'Instagrams'));
        $rules->add($rules->existsIn(['user_picture_id'], 'UserPictures'));
        $rules->add($rules->existsIn(['user_cover_id'], 'UserCovers'));
        $rules->add($rules->existsIn(['user_referrer_id'], 'UserReferrers'));
        $rules->add($rules->existsIn(['oraganisation_id'], 'Oraganisations'));

        return $rules;
    }
}
