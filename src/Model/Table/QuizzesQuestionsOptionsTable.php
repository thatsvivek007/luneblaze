<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * QuizzesQuestionsOptions Model
 *
 * @property \App\Model\Table\QuizzesTable&\Cake\ORM\Association\BelongsTo $Quizzes
 * @property \App\Model\Table\QuestionsTable&\Cake\ORM\Association\BelongsTo $Questions
 *
 * @method \App\Model\Entity\QuizzesQuestionsOption get($primaryKey, $options = [])
 * @method \App\Model\Entity\QuizzesQuestionsOption newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\QuizzesQuestionsOption[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\QuizzesQuestionsOption|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\QuizzesQuestionsOption saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\QuizzesQuestionsOption patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\QuizzesQuestionsOption[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\QuizzesQuestionsOption findOrCreate($search, callable $callback = null, $options = [])
 */
class QuizzesQuestionsOptionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('quizzes_questions_options');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Quizzes', [
            'foreignKey' => 'quiz_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('QuizzesQuestions', [
            'foreignKey' => 'question_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('option_text')
            ->maxLength('option_text', 255)
            ->requirePresence('option_text', 'create')
            ->notEmptyString('option_text');

        $validator
            ->boolean('is_correct')
            ->notEmptyString('is_correct');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    // public function buildRules(RulesChecker $rules)
    // {
    //     $rules->add($rules->existsIn(['quiz_id'], 'Quizzes'));
    //     $rules->add($rules->existsIn(['question_id'], 'QuizzesQuestions'));

    //     return $rules;
    // }
}
