<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Posts Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\GroupsTable&\Cake\ORM\Association\BelongsTo $Groups
 * @property \App\Model\Table\WallsTable&\Cake\ORM\Association\BelongsTo $Walls
 * @property \App\Model\Table\OriginsTable&\Cake\ORM\Association\BelongsTo $Origins
 * @property \App\Model\Table\OrganisationsTable&\Cake\ORM\Association\BelongsTo $Organisations
 * @property \App\Model\Table\QuizzesTable&\Cake\ORM\Association\BelongsTo $Quizzes
 * @property \App\Model\Table\ArticlesTable&\Cake\ORM\Association\BelongsToMany $Articles
 * @property \App\Model\Table\SessionsTable&\Cake\ORM\Association\BelongsToMany $Sessions
 *
 * @method \App\Model\Entity\Post get($primaryKey, $options = [])
 * @method \App\Model\Entity\Post newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Post[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Post|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Post saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Post patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Post[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Post findOrCreate($search, callable $callback = null, $options = [])
 */
class PostsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('posts');
        $this->setDisplayField('post_id');
        $this->setPrimaryKey('post_id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
        ]);
        $this->belongsTo('Walls', [
            'foreignKey' => 'wall_id',
        ]);
        $this->belongsTo('Origins', [
            'foreignKey' => 'origin_id',
        ]);
        $this->belongsTo('Organisations', [
            'foreignKey' => 'organisation_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Quizzes', [
            'foreignKey' => 'quiz_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsToMany('Articles', [
            'foreignKey' => 'post_id',
            'targetForeignKey' => 'article_id',
            'joinTable' => 'posts_articles',
        ]);
        $this->belongsToMany('Sessions', [
            'foreignKey' => 'post_id',
            'targetForeignKey' => 'session_id',
            'joinTable' => 'posts_sessions',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('post_id')
            ->allowEmptyString('post_id', null, 'create');

        $validator
            ->scalar('user_type')
            ->requirePresence('user_type', 'create')
            ->notEmptyString('user_type');

        // $validator
        //     ->scalar('in_group')
        //     ->notEmptyString('in_group');

        // $validator
        //     ->scalar('in_wall')
        //     ->notEmptyString('in_wall');

        $validator
            ->scalar('post_type')
            ->maxLength('post_type', 32)
            ->requirePresence('post_type', 'create')
            ->notEmptyString('post_type');

        // $validator
        //     ->notEmptyString('tagged');

        // $validator
        //     ->dateTime('time')
        //     ->requirePresence('time', 'create')
        //     ->notEmptyDateTime('time');

        // $validator
        //     ->dateTime('updated_at')
        //     ->allowEmptyDateTime('updated_at');

        // $validator
        //     ->scalar('location')
        //     ->maxLength('location', 255)
        //     ->allowEmptyString('location');

        // $validator
        //     ->scalar('privacy')
        //     ->maxLength('privacy', 32)
        //     ->requirePresence('privacy', 'create')
        //     ->notEmptyString('privacy');

        // $validator
        //     ->allowEmptyString('text');

        // $validator
        //     ->requirePresence('text_tagged', 'create')
        //     ->notEmptyString('text_tagged');

        // $validator
        //     ->scalar('feeling_action')
        //     ->maxLength('feeling_action', 32)
        //     ->allowEmptyString('feeling_action');

        // $validator
        //     ->scalar('feeling_value')
        //     ->maxLength('feeling_value', 255)
        //     ->allowEmptyString('feeling_value');

        // $validator
        //     ->scalar('boosted')
        //     ->notEmptyString('boosted');

        // $validator
        //     ->nonNegativeInteger('likes')
        //     ->notEmptyString('likes');

        // $validator
        //     ->nonNegativeInteger('comments')
        //     ->notEmptyString('comments');

        // $validator
        //     ->nonNegativeInteger('shares')
        //     ->notEmptyString('shares');

        // $validator
        //     ->integer('score')
        //     ->notEmptyString('score');

        // $validator
        //     ->notEmptyString('post_type_value');

        $validator
            ->scalar('post_title')
            ->requirePresence('post_title', 'create')
            ->notEmptyString('post_title');

        // $validator
        //     ->integer('total')
        //     ->requirePresence('total', 'create')
        //     ->notEmptyString('total');

        // $validator
        //     ->scalar('total_user_ids')
        //     ->maxLength('total_user_ids', 1000)
        //     ->allowEmptyString('total_user_ids');

        // $validator
        //     ->scalar('like_ids')
        //     ->maxLength('like_ids', 1000)
        //     ->allowEmptyString('like_ids');

        // $validator
        //     ->scalar('comment_ids')
        //     ->maxLength('comment_ids', 1000)
        //     ->allowEmptyString('comment_ids');

        // $validator
        //     ->scalar('origin_type')
        //     ->maxLength('origin_type', 100)
        //     ->requirePresence('origin_type', 'create')
        //     ->notEmptyString('origin_type');

        // $validator
        //     ->integer('payment')
        //     ->requirePresence('payment', 'create')
        //     ->notEmptyString('payment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    // public function buildRules(RulesChecker $rules)
    // {
    //     $rules->add($rules->existsIn(['user_id'], 'Users'));
    //     $rules->add($rules->existsIn(['group_id'], 'Groups'));
    //     $rules->add($rules->existsIn(['wall_id'], 'Walls'));
    //     $rules->add($rules->existsIn(['origin_id'], 'Origins'));
    //     $rules->add($rules->existsIn(['organisation_id'], 'Organisations'));
    //     $rules->add($rules->existsIn(['quiz_id'], 'Quizzes'));

    //     return $rules;
    // }
}
