<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $user_id
 * @property int $user_group
 * @property string $user_name
 * @property string $user_email
 * @property string|null $user_phone
 * @property string $user_password
 * @property string|null $current_FOI
 * @property string $user_latitude
 * @property string $user_longitude
 * @property string $user_activated
 * @property string|null $user_activation_key
 * @property string $user_reseted
 * @property string|null $user_reset_key
 * @property string $user_subscribed
 * @property int|null $user_package
 * @property \Cake\I18n\FrozenTime|null $user_subscription_date
 * @property int $user_boosted_posts
 * @property int $user_boosted_pages
 * @property string $user_started
 * @property string $user_verified
 * @property string $user_banned
 * @property int $user_live_requests_counter
 * @property int $user_live_requests_lastid
 * @property int $user_live_messages_counter
 * @property int $user_live_messages_lastid
 * @property int $user_live_notifications_counter
 * @property int $user_live_notifications_lastid
 * @property int $user_sessions_counter
 * @property string $user_firstname
 * @property string|null $user_lastname
 * @property string $user_fullname
 * @property string $user_gender
 * @property string $user_pic_dimensions
 * @property int|null $user_picture_id
 * @property string $user_cover_dimensions
 * @property int|null $user_cover_id
 * @property int|null $user_album_pictures
 * @property int|null $user_album_covers
 * @property int|null $user_album_timeline
 * @property int|null $user_pinned_post
 * @property \Cake\I18n\FrozenTime|null $user_registered
 * @property \Cake\I18n\FrozenTime|null $user_last_login
 * @property \Cake\I18n\FrozenDate|null $user_birthdate
 * @property string|null $user_relationship
 * @property string|null $user_biography
 * @property string|null $user_website
 * @property string $user_work
 * @property string|null $user_work_title
 * @property string|null $user_work_place
 * @property string|null $user_current_city
 * @property string|null $user_hometown
 * @property string|null $user_edu_major
 * @property string|null $user_edu_school
 * @property string|null $user_edu_class
 * @property string|null $user_social_facebook
 * @property string|null $user_social_twitter
 * @property string|null $user_social_google
 * @property string|null $user_social_youtube
 * @property string|null $user_social_instagram
 * @property string|null $user_social_linkedin
 * @property string|null $user_social_vkontakte
 * @property string $user_chat_enabled
 * @property string $user_privacy_wall
 * @property string $user_privacy_birthdate
 * @property string $user_privacy_relationship
 * @property string $user_privacy_basic
 * @property string $user_privacy_work
 * @property string $user_privacy_location
 * @property string $user_privacy_education
 * @property string $user_privacy_other
 * @property string $user_privacy_friends
 * @property string $user_privacy_photos
 * @property string $user_privacy_pages
 * @property string $user_privacy_groups
 * @property string $email_post_likes
 * @property string $email_post_comments
 * @property string $email_post_shares
 * @property string $email_wall_posts
 * @property string $email_mentions
 * @property string $email_profile_visits
 * @property string $email_friend_requests
 * @property string $facebook_connected
 * @property string|null $facebook_id
 * @property string $twitter_connected
 * @property string|null $twitter_id
 * @property string $google_connected
 * @property string|null $google_id
 * @property string $instagram_connected
 * @property string|null $instagram_id
 * @property string $linkedin_connected
 * @property string|null $linkedin_id
 * @property string $vkontakte_connected
 * @property string|null $vkontakte_id
 * @property int|null $user_referrer_id
 * @property string $user_affiliate_balance
 * @property string|null $fb_token
 * @property string|null $google_token
 * @property string|null $tw_token
 * @property string $privacy_connection_request
 * @property string $privacy_message
 * @property string $privacy_choices
 * @property string $privacy_content
 * @property string $privacy_tag
 * @property string $privacy_session
 * @property string $privacy_invite_session
 * @property string $privacy_invite_conduct_session
 * @property string $privacy_invite_answer
 * @property string $privacy_invite_interest
 * @property string $privacy_invite_venue
 * @property string $privacy_notification_email_invites_messages
 * @property string $privacy_notification_email_sessions
 * @property string $privacy_notification_email_qa
 * @property string $privacy_notification_email_articles
 * @property string $privacy_notification_email_update
 * @property string $privacy_notification_push_invites_messages
 * @property string $privacy_notification_push_sessions
 * @property string $privacy_notification_push_qa
 * @property string $privacy_notification_push_articles
 * @property string $privacy_notification_push_update
 * @property int $noti_sound
 * @property string $two_step_verifications
 * @property int $login_otp
 * @property string $verify_otp
 * @property int $people_influenced
 * @property \Cake\I18n\FrozenDate $delete_request_on
 * @property int $delete_withdraw
 * @property int $oraganisation_id
 * @property string $paymentplan
 * @property int $interest_block
 * @property int $quiz_count
 *
 * @property \App\Model\Entity\UserPicture $user_picture
 * @property \App\Model\Entity\UserCover $user_cover
 * @property \App\Model\Entity\Facebook $facebook
 * @property \App\Model\Entity\Twitter $twitter
 * @property \App\Model\Entity\Google $google
 * @property \App\Model\Entity\Instagram $instagram
 * @property \App\Model\Entity\Linkedin $linkedin
 * @property \App\Model\Entity\Vkontakte $vkontakte
 * @property \App\Model\Entity\UserReferrer $user_referrer
 * @property \App\Model\Entity\Oraganisation $oraganisation
 * @property \App\Model\Entity\Announcement[] $announcements
 * @property \App\Model\Entity\Conversation[] $conversations
 * @property \App\Model\Entity\CustomField[] $custom_fields
 * @property \App\Model\Entity\Session[] $sessions
 * @property \App\Model\Entity\SessionsPollsOption[] $sessions_polls_options
 * @property \App\Model\Entity\Venue[] $venue
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_group' => true,
        'user_name' => true,
        'user_email' => true,
        'user_phone' => true,
        'user_password' => true,
        'current_FOI' => true,
        'user_latitude' => true,
        'user_longitude' => true,
        'user_activated' => true,
        'user_activation_key' => true,
        'user_reseted' => true,
        'user_reset_key' => true,
        'user_subscribed' => true,
        'user_package' => true,
        'user_subscription_date' => true,
        'user_boosted_posts' => true,
        'user_boosted_pages' => true,
        'user_started' => true,
        'user_verified' => true,
        'user_banned' => true,
        'user_live_requests_counter' => true,
        'user_live_requests_lastid' => true,
        'user_live_messages_counter' => true,
        'user_live_messages_lastid' => true,
        'user_live_notifications_counter' => true,
        'user_live_notifications_lastid' => true,
        'user_sessions_counter' => true,
        'user_firstname' => true,
        'user_lastname' => true,
        'user_fullname' => true,
        'user_gender' => true,
        'user_picture' => true,
        'user_pic_dimensions' => true,
        'user_picture_id' => true,
        'user_cover' => true,
        'user_cover_dimensions' => true,
        'user_cover_id' => true,
        'user_album_pictures' => true,
        'user_album_covers' => true,
        'user_album_timeline' => true,
        'user_pinned_post' => true,
        'user_registered' => true,
        'user_last_login' => true,
        'user_birthdate' => true,
        'user_relationship' => true,
        'user_biography' => true,
        'user_website' => true,
        'user_work' => true,
        'user_work_title' => true,
        'user_work_place' => true,
        'user_current_city' => true,
        'user_hometown' => true,
        'user_edu_major' => true,
        'user_edu_school' => true,
        'user_edu_class' => true,
        'user_social_facebook' => true,
        'user_social_twitter' => true,
        'user_social_google' => true,
        'user_social_youtube' => true,
        'user_social_instagram' => true,
        'user_social_linkedin' => true,
        'user_social_vkontakte' => true,
        'user_chat_enabled' => true,
        'user_privacy_wall' => true,
        'user_privacy_birthdate' => true,
        'user_privacy_relationship' => true,
        'user_privacy_basic' => true,
        'user_privacy_work' => true,
        'user_privacy_location' => true,
        'user_privacy_education' => true,
        'user_privacy_other' => true,
        'user_privacy_friends' => true,
        'user_privacy_photos' => true,
        'user_privacy_pages' => true,
        'user_privacy_groups' => true,
        'email_post_likes' => true,
        'email_post_comments' => true,
        'email_post_shares' => true,
        'email_wall_posts' => true,
        'email_mentions' => true,
        'email_profile_visits' => true,
        'email_friend_requests' => true,
        'facebook_connected' => true,
        'facebook_id' => true,
        'twitter_connected' => true,
        'twitter_id' => true,
        'google_connected' => true,
        'google_id' => true,
        'instagram_connected' => true,
        'instagram_id' => true,
        'linkedin_connected' => true,
        'linkedin_id' => true,
        'vkontakte_connected' => true,
        'vkontakte_id' => true,
        'user_referrer_id' => true,
        'user_affiliate_balance' => true,
        'fb_token' => true,
        'google_token' => true,
        'tw_token' => true,
        'privacy_connection_request' => true,
        'privacy_message' => true,
        'privacy_choices' => true,
        'privacy_content' => true,
        'privacy_tag' => true,
        'privacy_session' => true,
        'privacy_invite_session' => true,
        'privacy_invite_conduct_session' => true,
        'privacy_invite_answer' => true,
        'privacy_invite_interest' => true,
        'privacy_invite_venue' => true,
        'privacy_notification_email_invites_messages' => true,
        'privacy_notification_email_sessions' => true,
        'privacy_notification_email_qa' => true,
        'privacy_notification_email_articles' => true,
        'privacy_notification_email_update' => true,
        'privacy_notification_push_invites_messages' => true,
        'privacy_notification_push_sessions' => true,
        'privacy_notification_push_qa' => true,
        'privacy_notification_push_articles' => true,
        'privacy_notification_push_update' => true,
        'noti_sound' => true,
        'two_step_verifications' => true,
        'login_otp' => true,
        'verify_otp' => true,
        'people_influenced' => true,
        'delete_request_on' => true,
        'delete_withdraw' => true,
        'oraganisation_id' => true,
        'paymentplan' => true,
        'interest_block' => true,
        'quiz_count' => true,
        'facebook' => true,
        'twitter' => true,
        'google' => true,
        'instagram' => true,
        'linkedin' => true,
        'vkontakte' => true,
        'user_referrer' => true,
        'oraganisation' => true,
        'announcements' => true,
        'conversations' => true,
        'custom_fields' => true,
        'sessions' => true,
        'sessions_polls_options' => true,
        'venue' => true,
    ];
}
