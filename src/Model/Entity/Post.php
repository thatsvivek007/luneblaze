<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Post Entity
 *
 * @property int $post_id
 * @property int $user_id
 * @property string $user_type
 * @property string $in_group
 * @property int|null $group_id
 * @property string $in_wall
 * @property int|null $wall_id
 * @property string $post_type
 * @property int $tagged
 * @property int|null $origin_id
 * @property \Cake\I18n\FrozenTime $time
 * @property \Cake\I18n\FrozenTime|null $updated_at
 * @property string|null $location
 * @property string $privacy
 * @property string|resource|null $text
 * @property string|resource $text_tagged
 * @property string|null $feeling_action
 * @property string|null $feeling_value
 * @property string $boosted
 * @property int $likes
 * @property int $comments
 * @property int $shares
 * @property int $score
 * @property int $post_type_value
 * @property string $post_title
 * @property int $total
 * @property string|null $total_user_ids
 * @property string|null $like_ids
 * @property string|null $comment_ids
 * @property string $origin_type
 * @property int $organisation_id
 * @property int $payment
 * @property int $quiz_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Group $group
 * @property \App\Model\Entity\Wall $wall
 * @property \App\Model\Entity\Origin $origin
 * @property \App\Model\Entity\Organisation $organisation
 * @property \App\Model\Entity\Quiz $quiz
 * @property \App\Model\Entity\Article[] $articles
 * @property \App\Model\Entity\Session[] $sessions
 */
class Post extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'user_type' => true,
        'in_group' => true,
        'group_id' => true,
        'in_wall' => true,
        'wall_id' => true,
        'post_type' => true,
        'tagged' => true,
        'origin_id' => true,
        'time' => true,
        'updated_at' => true,
        'location' => true,
        'privacy' => true,
        'text' => true,
        'text_tagged' => true,
        'feeling_action' => true,
        'feeling_value' => true,
        'boosted' => true,
        'likes' => true,
        'comments' => true,
        'shares' => true,
        'score' => true,
        'post_type_value' => true,
        'post_title' => true,
        'total' => true,
        'total_user_ids' => true,
        'like_ids' => true,
        'comment_ids' => true,
        'origin_type' => true,
        'organisation_id' => true,
        'payment' => true,
        'quiz_id' => true,
        'user' => true,
        'group' => true,
        'wall' => true,
        'origin' => true,
        'organisation' => true,
        'quiz' => true,
        'articles' => true,
        'sessions' => true,
    ];
}
