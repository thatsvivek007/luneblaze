<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Quiz Entity
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $picture
 * @property string $user_type
 * @property int $creator_id
 * @property \Cake\I18n\FrozenTime $start_time
 * @property int $live_duration
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Creator $creator
 * @property \App\Model\Entity\Post[] $posts
 * @property \App\Model\Entity\QuizzesQuestion[] $quizzes_questions
 * @property \App\Model\Entity\QuizzesQuestionsOption[] $quizzes_questions_options
 */
class Quiz extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'description' => true,
        'picture' => true,
        'user_type' => true,
        'creator_id' => true,
        'start_time' => true,
        'live_duration' => true,
        'created' => true,
        'modified' => true,
        'creator' => true,
        'posts' => true,
        'quizzes_questions' => true,
        'quizzes_questions_options' => true,
    ];
}
