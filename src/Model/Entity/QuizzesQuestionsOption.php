<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * QuizzesQuestionsOption Entity
 *
 * @property int $id
 * @property int $quiz_id
 * @property int $question_id
 * @property string $option_text
 * @property bool $is_correct
 *
 * @property \App\Model\Entity\Quiz $quiz
 * @property \App\Model\Entity\Question $question
 */
class QuizzesQuestionsOption extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'quiz_id' => true,
        'question_id' => true,
        'option_text' => true,
        'is_correct' => true,
        'quiz' => true,
        'question' => true,
    ];
}
