<?php

namespace App\Controller;
use App\Controller\AppController;
use Cake\Validation\Validator;
use App\Utils\DataValidation;
// use App\Utils\Codes;

class QuizzesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function index()
    {   
        $quizzes = $this->Quizzes->find('all');

        $this->set([
            'status' => 200,
            'message' => "",
            'data' => $quizzes,
            '_serialize' => ['status','message','data']
        ]);
    }

    public function view($id)
    {
        $quiz = $this->Quizzes->get($id);

        $this->set([
            'status' => ($quiz)?200:404,
            'message' => "",
            'data' => $quiz,
            '_serialize' => ['status','message','data']
        ]);
    }

    public function add()
    {
        $this->request->allowMethod(['post', 'put']);

        $this->loadModel("Users");
        $this->loadModel("Posts");
        $this->loadModel("QuizzesQuestions");
        $this->loadModel("QuizzesQuestionsOptions");


        $validator = new Validator();
        $validator
            ->requirePresence('title', 'Title is required.')
            ->notEmpty('title', 'Title is required.')
            ->requirePresence('description')
            ->notEmpty('description', 'Description is required')
            ->requirePresence('start_time')
            ->notEmpty('start_time', 'Start time is required.')
            ->requirePresence('live_duration')
            ->notEmpty('live_duration', 'Live duration is required.')
            ->requirePresence('creator_id')
            ->notEmpty('Questionlist_item', 'Questions is required.')
            ->requirePresence('creator_id')
            ->notEmpty('Questionlist_item', 'Questions is required.');


        $errors = $validator->errors($this->request->getData());
        if (empty($errors)) {
            $quiz = $this->Quizzes->newEntity($this->request->getData());
            if ($this->Quizzes->save($quiz)) {

                $cudata = $this->Posts->newEntity($this->request->getData());
                $cudata->user_id = 1;
                $cudata->post_title= $this->request->data["title"];
                $cudata->quiz_id= @$quiz->id;
                $cudata->post_type= 'quiz';
                $cudata->privacy= 'public';
                $cudata->text_tagged= '';
                $cudata->origin_type= '';
                $cudata->organisation_id= 0;
                $cudata->payment= 0;
                $cudata->total= 0;
                $cudata->updated_at = date("Y-m-d H:s:i");
                $cudata->time = date("Y-m-d H:s:i");
                $this->Posts->save($cudata);
                $questionData = json_decode($this->request->data["questionlist_item"]);
                foreach ($questionData as $key => $question) {
                    // print_r($question); exit;
                    $qdata = $this->QuizzesQuestions->newEntity($this->request->getData());
                    $qdata->question = $question->question;
                    $qdata->time_alloted = $question->time_alloted;
                    $qdata->picture = $question->picture;
                    $qdata->quiz_id = $quiz->id;

                    $this->QuizzesQuestions->save($qdata);
                    if($question->options){
                        foreach ($question->options as $key => $optionRow) {
                            $odata = $this->QuizzesQuestionsOptions->newEntity((array)$optionRow);
                            $odata->quiz_id = $quiz->id;
                            $odata->question_id = $qdata->id;
                            $this->QuizzesQuestionsOptions->save($odata);
                        }

                    }
                }

                $query = $this->Quizzes->find();
                $query->select(['count' => $query->func()->count('id')]);
                $query->where(['Quizzes.creator_id'=>1]);
                $assignment = $query->toArray();
                if(isset($assignment[0]->count) && $assignment[0]->count > 0){
                    $query = $this->Users->query();
                    $query->update()
                        ->set(['quiz_count' => $assignment[0]->count])
                        ->where(['user_id' => 1])
                        ->execute();
                }

                $message = 'Saved';
            } else {
                $message = 'Error';
            }
        }else{
            $message = $errors;
        }

        $this->set([
            'status' => (!$errors)?200:400,
            'message' => $message,
            'data' => @$quiz,
            '_serialize' => ['status','message','data']
        ]);
    }

    public function edit($id)
    {
        $this->request->allowMethod(['patch', 'post', 'put']);
        $recipe = $this->Quizzes->get($id);
        $recipe = $this->Quizzes->patchEntity($recipe, $this->request->getData());
        if ($this->Quizzes->save($recipe)) {
            $message = 'Saved';
        } else {
            $message = 'Error';
        }
        $this->set([
            'message' => $message,
            '_serialize' => ['message']
        ]);
    }

    public function delete($id)
    {
        $this->request->allowMethod(['delete']);
        $recipe = $this->Quizzes->get($id);
        $message = 'Deleted';
        if (!$this->Quizzes->delete($recipe)) {
            $message = 'Error';
        }
        $this->set([
            'message' => $message,
            '_serialize' => ['message']
        ]);
    }
}