-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 03, 2020 at 11:35 AM
-- Server version: 5.7.26
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `luneblaze`
--

-- --------------------------------------------------------

--
-- Table structure for table `quizzes`
--

DROP TABLE IF EXISTS `quizzes`;
CREATE TABLE IF NOT EXISTS `quizzes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `picture` varchar(255) NOT NULL,
  `user_type` varchar(30) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `live_duration` int(11) NOT NULL COMMENT 'in minutes',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quizzes`
--

INSERT INTO `quizzes` (`id`, `title`, `description`, `picture`, `user_type`, `creator_id`, `start_time`, `live_duration`, `created`, `modified`) VALUES
(1, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 09:04:43', '2020-10-03 09:04:43'),
(2, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:02:12', '2020-10-03 10:02:12'),
(3, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:14:17', '2020-10-03 10:14:17'),
(4, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:41:20', '2020-10-03 10:41:20'),
(5, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:41:56', '2020-10-03 10:41:56'),
(6, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:43:29', '2020-10-03 10:43:29'),
(7, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:43:42', '2020-10-03 10:43:42'),
(8, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:43:57', '2020-10-03 10:43:57'),
(9, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:44:09', '2020-10-03 10:44:09'),
(10, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:44:19', '2020-10-03 10:44:19'),
(11, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:44:30', '2020-10-03 10:44:30'),
(12, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:45:37', '2020-10-03 10:45:37'),
(13, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:46:30', '2020-10-03 10:46:30'),
(14, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:46:36', '2020-10-03 10:46:36'),
(15, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:48:35', '2020-10-03 10:48:35'),
(16, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:48:52', '2020-10-03 10:48:52'),
(17, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:49:19', '2020-10-03 10:49:19'),
(18, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:49:36', '2020-10-03 10:49:36'),
(19, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:50:19', '2020-10-03 10:50:19'),
(20, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:50:27', '2020-10-03 10:50:27'),
(21, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:51:02', '2020-10-03 10:51:02'),
(22, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:51:51', '2020-10-03 10:51:51'),
(23, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:52:30', '2020-10-03 10:52:30'),
(24, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:52:57', '2020-10-03 10:52:57'),
(25, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:53:59', '2020-10-03 10:53:59'),
(26, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:54:59', '2020-10-03 10:54:59'),
(27, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:57:27', '2020-10-03 10:57:27'),
(28, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 10:59:39', '2020-10-03 10:59:39'),
(29, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 11:00:25', '2020-10-03 11:00:25'),
(30, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 11:01:56', '2020-10-03 11:01:56'),
(31, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 11:02:37', '2020-10-03 11:02:37'),
(32, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 11:02:59', '2020-10-03 11:02:59'),
(33, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 11:03:19', '2020-10-03 11:03:19'),
(34, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 11:03:35', '2020-10-03 11:03:35'),
(35, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 11:03:59', '2020-10-03 11:03:59'),
(36, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 11:04:26', '2020-10-03 11:04:26'),
(37, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 11:04:45', '2020-10-03 11:04:45'),
(38, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 11:05:33', '2020-10-03 11:05:33'),
(39, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 11:05:57', '2020-10-03 11:05:57'),
(40, 'xzxz', 'dfgdfg', '', 'User', 1, '2020-09-20 10:10:00', 10, '2020-10-03 11:22:33', '2020-10-03 11:22:33');

-- --------------------------------------------------------

--
-- Table structure for table `quizzes_questions`
--

DROP TABLE IF EXISTS `quizzes_questions`;
CREATE TABLE IF NOT EXISTS `quizzes_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `picture` int(255) DEFAULT NULL,
  `time_alloted` int(11) NOT NULL COMMENT 'in minutes',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quizzes_questions`
--

INSERT INTO `quizzes_questions` (`id`, `quiz_id`, `question`, `picture`, `time_alloted`) VALUES
(1, 20, 'This is my question?', NULL, 20),
(2, 21, 'This is my question?', NULL, 20),
(3, 23, 'This is my question?', NULL, 20),
(4, 24, 'This is my question?', NULL, 20),
(5, 25, 'This is my question?', NULL, 20),
(6, 26, 'This is my question?', NULL, 20),
(7, 27, 'This is my question?', NULL, 20),
(8, 28, 'This is my question?', NULL, 20),
(9, 29, 'This is my question?', NULL, 20),
(10, 30, 'This is my question?', NULL, 20),
(11, 31, 'This is my question?', NULL, 20),
(12, 32, 'This is my question?', NULL, 20),
(13, 33, 'This is my question?', NULL, 20),
(14, 34, 'This is my question?', NULL, 20),
(15, 35, 'This is my question?', NULL, 20),
(16, 36, 'This is my question?', NULL, 20),
(17, 37, 'This is my question?', NULL, 20),
(18, 38, 'This is my question?', NULL, 20),
(19, 39, 'This is my question?', NULL, 20),
(20, 40, 'This is my question?', NULL, 20),
(21, 41, 'This is my question?', NULL, 20);

-- --------------------------------------------------------

--
-- Table structure for table `quizzes_questions_options`
--

DROP TABLE IF EXISTS `quizzes_questions_options`;
CREATE TABLE IF NOT EXISTS `quizzes_questions_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_text` varchar(255) NOT NULL,
  `is_correct` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quizzes_questions_options`
--

INSERT INTO `quizzes_questions_options` (`id`, `quiz_id`, `question_id`, `option_text`, `is_correct`) VALUES
(1, 28, 8, 'Option 1', 0),
(2, 29, 9, 'Option 1', 0),
(3, 30, 10, 'Option 1', 0),
(4, 32, 12, 'Option 1', 0),
(5, 35, 15, 'Option 1', 0),
(6, 36, 16, 'Option 1', 0),
(7, 39, 19, 'Option 1', 0),
(8, 40, 20, 'Option 1', 0),
(9, 41, 21, 'Option 1', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
